﻿using UnityEngine;

namespace Assets.Scripts
{
    public class FinishTile : Tile
    {
        public override void OnCollisionEnter(Collision collision)
        {
            GameManager.Instance.SwitchState(GameManager.State.LEVELCOMPLETE);
        }

        public override void Start()
        {
            base.Start();
            _currentTileInfo.text = "";
        }

    }
}
