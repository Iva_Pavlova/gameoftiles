﻿using Assets.Scripts;
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public abstract class Tile : MonoBehaviour
{
    private const int _visibilityDistanceRows = 3;

    protected static readonly System.Random _random = new System.Random();

    protected Text _currentTileInfo;
    protected Renderer _renderer;
    protected Renderer _tileInfoRenderer;

    public Text TileInfoPrefab;

    public abstract void OnCollisionEnter(Collision collision);

    public virtual void Start()
    {
        _renderer = GetComponent<Renderer>();
        _currentTileInfo = Instantiate(this.TileInfoPrefab, GameManager.Instance.CurrentCanvasTileInfo.transform);
    }

    public virtual void Update()
    {
        _currentTileInfo.transform.position = Camera.main.WorldToScreenPoint(this.transform.position);
        this.UpdateVisibility();
    }

    private bool TileIsCloseToPlayer()
    {
        var distance = Math.Abs(GameManager.Instance.CurrentPlayer.transform.position.z - this.transform.position.z);
        return distance < _visibilityDistanceRows * GameField.DistanceBetweenTileCenters;
    }

    private void UpdateVisibility()
    {
        if (this.TileIsCloseToPlayer() && _renderer.enabled == false)
        {
            _renderer.enabled = true;
            _currentTileInfo.enabled = true;
        }

        else if (!this.TileIsCloseToPlayer())
        {
            _renderer.enabled = false;
            _currentTileInfo.enabled = false;
        }
    }

}
