﻿using UnityEngine;

namespace Assets.Scripts
{
    public class CameraFollow : MonoBehaviour
    {
        private const float _offsetOnZ = 2.25f;
        private const float _speed = 2.0f;

        public GameObject _target;

        public void LateUpdate()
        {
            if (!this.TryGetTarget())
                return;

            float interpolation = _speed * Time.deltaTime;

            Vector3 position = this.transform.position;
            position.z = Mathf.Lerp(this.transform.position.z, _target.transform.position.z - _offsetOnZ, interpolation);

            this.transform.position = position;
        }

        private GameObject FindPlayerObject()
        {
            return GameObject.Find("Player(Clone)");
        }

        private bool TryGetTarget()
        {
            if (_target != null)
                return true;

            var player = this.FindPlayerObject();
            if (player == null)
                return false;

            _target = player;
            return true;
        }

    }
}
