﻿using UnityEngine;

namespace Assets.Scripts
{
    public class FountainTile : Tile
    {
        private int _recoveryValue;

        public int RecoveryValue { get => _recoveryValue; }

        public override void OnCollisionEnter(Collision collision)
        {
            if (collision.collider.TryGetComponent<Player>(out Player player))
                this.HealPlayer(player);
        }

        public override void Start()
        {
            _recoveryValue = _random.Next(10, 31); // ToDo 
            base.Start();
            _currentTileInfo.text = $" + {_recoveryValue} HP";
        }

        private void HealPlayer(Player player)
        {
            player.Health += this.RecoveryValue;
        }
    }
}
