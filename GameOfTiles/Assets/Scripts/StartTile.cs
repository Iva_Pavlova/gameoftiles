﻿using UnityEngine;

namespace Assets.Scripts
{
    public class StartTile : Tile
    {
        public override void OnCollisionEnter(Collision collision)
        {
        }

        public override void Start()
        {
            base.Start();
            this._currentTileInfo.text = "";
        }

    }
}
