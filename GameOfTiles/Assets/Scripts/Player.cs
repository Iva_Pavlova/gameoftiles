﻿using System;
using UnityEngine;
using static Assets.Scripts.GameManager;

namespace Assets.Scripts
{
    public class Player : MonoBehaviour
    {
        private int _damage;
        private bool _grounded;
        private int _health;
        private int _maxHealth;

        private Rigidbody _rigidbody;
        private Renderer _renderer;

        public int Health
        {
            get => _health;
            set
            {
                if (value <= 0)
                {
                    value = 0;
                    GameManager.Instance.SwitchState(GameManager.State.GAMEOVER);
                }

                else if (value > this.MaxHealth)
                    value = this.MaxHealth;

                _health = value;
                GameManager.Instance.TextHealth.text = value.ToString();
            }
        }

        public int MaxHealth
        {
            get => _maxHealth;
            set
            {
                _maxHealth = value;
                GameManager.Instance.TextMaxHealth.text = $"/ {value}";
            }
        }

        public int Damage
        {
            get => _damage;
            set
            {
                _damage = value;
                GameManager.Instance.TextDamage.text = value.ToString();
            }
        }


        public void FixedUpdate()
        {
            this.CheckIfFallen();
        }

        public void OnCollisionEnter(Collision collision)
        {
            _grounded = true;
        }

        public void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _renderer = this.GetComponent<Renderer>();

            this.MaxHealth = 100;
            this.Health = this.MaxHealth;
            this.Damage = 20;
        }

        public void Update()
        {
            switch (GameManager.Instance.CurrentState)
            {
                case State.PLAY:
                    this.ReadKeyboard();
                    break;

                case State.LEVELCOMPLETE:
                    this.Jump();
                    break;
            }
        }

        private void MoveLeft()
        {
            var position = _rigidbody.transform.position;
            _rigidbody.MovePosition(new Vector3(position.x - GameField.DeplacementToTheSide * GameField.DistanceBetweenTileCenters, position.y, position.z + GameField.DistanceBetweenTileCenters));
        }

        private void MoveRight()
        {
            var position = _rigidbody.transform.position;
            _rigidbody.MovePosition(new Vector3(position.x + GameField.DeplacementToTheSide * GameField.DistanceBetweenTileCenters, position.y, position.z + GameField.DistanceBetweenTileCenters));
        }

        private void ReadKeyboard()
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
                this.MoveLeft();

            else if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
                this.MoveRight();
        }

        private void CheckIfFallen()
        {
            if (this.transform.position.y < 0.3f)
                GameManager.Instance.SwitchState(GameManager.State.GAMEOVER);
        }

        private void Jump()
        {
            if (_grounded)
            {
                _rigidbody.velocity = Vector3.up * 3f;
                _grounded = false;
            }
        }

    }
}
