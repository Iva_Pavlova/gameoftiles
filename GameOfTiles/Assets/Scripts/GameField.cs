﻿using UnityEngine;

namespace Assets.Scripts
{
    public class GameField : MonoBehaviour
    {
        private const int _constantFieldLength = 11; // should be odd
        private const int _numOfShops = 3;
        private const int _numOfFountains = 2;

        private int _length;
        private GameObject[][] _tiles;

        public const float DistanceBetweenTileCenters = 2f; // ToDo : refactor
        public const float DeplacementToTheSide = 0.5f;

        public GameObject EnemyTile;
        public GameObject FinishTile;
        public GameObject FountainTile;
        public GameObject ShopTile;
        public GameObject StartTile;

        public int Legth
        {
            get => _length;
            private set
            {
                if (!this.ValueIsOdd(value))
                    value++; // otherwise the last row before the Finish Tile will have 3 Tiles and will be impossible to pass

                _length = value;
            }
        }

        public GameObject[][] Tiles { get => _tiles; }

        public void Start()
        {
            this.Legth = _constantFieldLength;
            _tiles = new GameObject[this.Legth][];
            this.CreateTileSlots();
            this.GenerateTiles();
        }

        private void AddEnemyTiles()
        {
            for (int row = 1; row < this.Legth - 1; row++)
            {
                for (int col = 0; col < _tiles[row].Length; col++)
                {
                    if (_tiles[row][col] == null)
                    {
                        _tiles[row][col] = this.InstantiateTile(this.EnemyTile, row, col);
                    }
                }
            }
        }

        private void AddStartAndFinishTiles()
        {
            _tiles[0][0] = this.InstantiateTile(this.StartTile, 0, 0);
            _tiles[this.Legth - 1][0] = this.InstantiateTile(this.FinishTile, this.Legth - 1, 0);
        }

        private void AddTilesOfSpecialType(GameObject type, int requiredNumOfTiles)
        {
            var rnd = new System.Random();
            bool[] rowsWithSuchTile = new bool[this.Legth];

            for (int i = 0; i < requiredNumOfTiles; i++)
            {
                this.FindSuitableRowForTile(rowsWithSuchTile, rnd, out int rowIndex);
                this.AddTileOfTypeToRow(type, rowIndex, rnd);

                rowsWithSuchTile[rowIndex] = true;
            }
        }

        private void AddTileOfTypeToRow(GameObject type, int indexOfRow, System.Random rnd)
        {
            var column = rnd.Next(0, _tiles[indexOfRow].Length);

            while (_tiles[indexOfRow][column] != null)
            {
                column = rnd.Next(0, _tiles[indexOfRow].Length);
            }

            _tiles[indexOfRow][column] = Instantiate(type, this.DeterminePosition(indexOfRow, column), new Quaternion(), this.transform);
        }

        private void CreateNewRow(int indexOfRow)
        {
            if (this.NumOfRowIsEven(indexOfRow))
                _tiles[indexOfRow] = new GameObject[2];
            else
                _tiles[indexOfRow] = new GameObject[3];
        }

        private void CreateTileSlots()
        {
            _tiles[0] = new GameObject[1];

            for (int i = 1; i < this.Legth - 1; i++)
            {
                this.CreateNewRow(i);
            }

            _tiles[this.Legth - 1] = new GameObject[1];
        }

        private Vector3 DeterminePosition(int row, int col)     // we use this to determine the place of Tiles in the 3D world
        {
            float deplacementToTheLeft;

            if (row == 0 || row == this.Legth - 1)
                deplacementToTheLeft = 0;

            else if (this.NumOfRowIsEven(row))
                deplacementToTheLeft = GameField.DeplacementToTheSide * GameField.DistanceBetweenTileCenters;

            else
                deplacementToTheLeft = GameField.DistanceBetweenTileCenters;

            return new Vector3(col * GameField.DistanceBetweenTileCenters - deplacementToTheLeft, 0, row * GameField.DistanceBetweenTileCenters);
        }

        private void FindSuitableRowForTile(bool[] rowsWithSuchTile, System.Random rnd, out int rowIndex)
        {
            rowIndex = rnd.Next(1, this.Legth - 1);

            while (this.RowHasSuchTile(rowIndex, rowsWithSuchTile))
            {
                rowIndex = rnd.Next(1, this.Legth - 1);
            }
        }

        private void GenerateTiles()
        {
            this.AddStartAndFinishTiles();
            this.AddTilesOfSpecialType(this.ShopTile, _numOfShops);
            this.AddTilesOfSpecialType(this.FountainTile, _numOfFountains);
            this.AddEnemyTiles();
        }

        private GameObject InstantiateTile(GameObject type, int row, int col)
        {
            return Instantiate(type, this.DeterminePosition(row, col), new Quaternion(), transform);
        }

        private bool NumOfRowIsEven(int indexOfRow)
        {
            return (indexOfRow + 1) % 2 == 0;
        }

        private bool RowHasSuchTile(int indexOfRow, bool[] rowsWithSuchTiles)
        {
            return rowsWithSuchTiles[indexOfRow];
        }

        private bool ValueIsOdd(int value)
        {
            return value % 2 == 1;
        }

    }
}
