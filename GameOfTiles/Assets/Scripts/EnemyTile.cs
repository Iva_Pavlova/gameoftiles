﻿using System;
using UnityEngine;

namespace Assets.Scripts
{
    public class EnemyTile : Tile
    {
        private const double _damageCoefficient = 0.4d;
        private int _damage;

        public int Damage { get => _damage; }

        public override void OnCollisionEnter(Collision collision)
        {
            if (collision.collider.TryGetComponent<Player>(out Player player))
                this.AttackPlayer(player);
        }

        public override void Start()
        {
            _damage = _random.Next(5, 51);
            base.Start();
            _currentTileInfo.text = _damage + " DMG";
        }

        private void AttackPlayer(Player player)
        {
            double chanceToWin = (double)this.Damage / (double)player.Damage;
            double damageToInflict = (double)this.Damage * (double)chanceToWin * _damageCoefficient;

            player.Health -= (int)Math.Round(damageToInflict); // ToDo : Fix Logic
        }
    }
}
