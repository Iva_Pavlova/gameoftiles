﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class ShopTile : Tile
    {
        private int _damageIncreaseValue;
        private int _healValue;
        private int _maxHealthIncreaseValue;

        private enum _function { HEALTH, DAMAGE, MAXHEALTH }
        private Player _player;
        private Action<Player> _specialFunction;

        public int DamageIncreaseValue { get => _damageIncreaseValue; }
        public int HealthRecoveryValue { get => _healValue; }
        public int MaxHealthIncreaseValue { get => _maxHealthIncreaseValue; }


        public override void OnCollisionEnter(Collision collision)
        {
            if (!collision.collider.TryGetComponent<Player>(out Player player))
                return;

            _player = player;

            this.AssignButtonValues();
            GameManager.Instance.SwitchState(GameManager.State.SHOP);
        }

        public override void Start()
        {
            base.Start();

            _healValue = _random.Next(15, 31);
            _damageIncreaseValue = _random.Next(5, 16);
            _maxHealthIncreaseValue = _random.Next(10, 26);

            this._currentTileInfo.text = "SHOP";
        }


        private void AssignButtonValues()
        {
            this.UpdateHealButton();
            this.UpdateDamageButton();
            this.UpdateMaxHealthButton();
        }

        private Button GetButtonByName(string name)
        {
            return GameManager.Instance.PanelShop.transform.Find(name).GetComponent<Button>();
        }

        private void IncreaseDamage(Player player)
        {
            player.Damage += this.DamageIncreaseValue;
        }

        private void IncreaseMaxHealth(Player player)
        {
            player.MaxHealth += this.MaxHealthIncreaseValue;
        }

        private void InvokeSpecialFunction(_function func)
        {
            switch (func)
            {
                case _function.HEALTH:
                    _specialFunction = this.RecoverHealth;
                    break;

                case _function.DAMAGE:
                    _specialFunction = this.IncreaseDamage;
                    break;

                case _function.MAXHEALTH:
                    _specialFunction = this.IncreaseMaxHealth;
                    break;

                default:
                    break;
            }

            _specialFunction.Invoke(_player);
            GameManager.Instance.SwitchState(GameManager.State.PLAY);
        }

        private void RecoverHealth(Player player)
        {
            player.Health += this.HealthRecoveryValue;
        }

        private void UpdateDamageButton()
        {
            var increaseDamageButton = this.GetButtonByName("ButtonIncreaseDamage");
            increaseDamageButton.GetComponentInChildren<Text>().text = $"+{_damageIncreaseValue} DMG";
            increaseDamageButton.onClick.RemoveAllListeners();
            increaseDamageButton.onClick.AddListener(() => this.InvokeSpecialFunction(_function.DAMAGE));
        }

        private void UpdateHealButton()
        {
            var healButton = this.GetButtonByName("ButtonHeal");
            healButton.GetComponentInChildren<Text>().text = $"+{_healValue} HP";
            healButton.onClick.RemoveAllListeners();
            healButton.onClick.AddListener(() => this.InvokeSpecialFunction(_function.HEALTH));
        }

        private void UpdateMaxHealthButton()
        {
            var increaseMaxHealthButton = this.GetButtonByName("ButtonIncreaseMaxHealth");
            increaseMaxHealthButton.GetComponentInChildren<Text>().text = $"+{_maxHealthIncreaseValue} \r\nMax Health";
            increaseMaxHealthButton.onClick.RemoveAllListeners();
            increaseMaxHealthButton.onClick.AddListener(() => this.InvokeSpecialFunction(_function.MAXHEALTH));
        }
    }
}
