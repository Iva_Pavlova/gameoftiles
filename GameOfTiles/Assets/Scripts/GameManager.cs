﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class GameManager : MonoBehaviour
    {
        private State _currentState;
        private Vector3 _initialCameraPosition;
        private bool _isSwitchingState;
        private State _previousState;

        public GameObject Camera;

        public Canvas CanvasTileInfo;
        public Canvas CurrentCanvasTileInfo;

        public GameObject CurrentField;
        public GameObject CurrentPlayer;

        public GameObject GameFieldPrefab;

        public GameObject PanelControls;
        public GameObject PanelMenu;
        public GameObject PanelPlay;
        public GameObject PanelShop;
        public GameObject PanelLevelComplete;
        public GameObject PanelGameOver;
        public GameObject PanelExit;

        public GameObject PlayerPrefab;

        public Text TextDamage;
        public Text TextHealth;
        public Text TextMaxHealth;

        public static GameManager Instance { get; private set; }

        public enum State { MENU, INIT, PLAY, SHOP, LEVELCOMPLETE, GAMEOVER, EXIT, CONTROLS }

        public State CurrentState { get => _currentState; }


        private void Start()
        {
            Instance = this;
            _initialCameraPosition = this.Camera.transform.position;
            this.SwitchState(State.MENU);
        }

        private void Update()
        {
            if (_isSwitchingState)
                return;

            if (Input.GetKeyUp(KeyCode.Escape))
                this.ExitClicked();

            switch (_currentState)
            {
                case State.EXIT:
                    this.ReadKeyboardForExit();
                    break;

                case State.GAMEOVER:
                case State.LEVELCOMPLETE:
                    this.WaitForAnyKey();
                    break;

                default:
                    break;
            }
        }


        public void ControlsClicked()
        {
            this.SwitchState(State.CONTROLS);
        } 

        public void ExitClicked()
        {
            this.SwitchState(State.EXIT);
        }

        public void MenuClicked()
        {
            this.SwitchState(State.MENU);
        } 

        public void PlayClicked()
        {
            this.SwitchState(State.INIT);
        }

        public void SwitchState(State newState)
        {
            _isSwitchingState = true;

            this.EndState();
            _previousState = _currentState;
            _currentState = newState;
            this.BeginState(newState);

            _isSwitchingState = false;
        }


        private void BeginState(State newState)
        {
            switch (newState)
            {
                case State.MENU:
                    this.PanelMenu.SetActive(true);
                    break;
                
                case State.CONTROLS:
                    this.PanelControls.SetActive(true);
                    break;

                case State.INIT:
                    this.Initialize();
                    this.SwitchState(State.PLAY);
                    break;

                case State.PLAY:
                    this.PanelPlay.SetActive(true);
                    break;

                case State.LEVELCOMPLETE:
                    this.PanelLevelComplete.SetActive(true);
                    break;

                case State.SHOP:
                    this.PanelShop.SetActive(true);
                    break;

                case State.GAMEOVER:
                    this.PanelGameOver.SetActive(true);
                    break;

                case State.EXIT:
                    this.PanelExit.SetActive(true);
                    break;

                default:
                    break;
            }

        }

        private void DestroyGameObjects()
        {
            Destroy(CurrentField);
            Destroy(CurrentCanvasTileInfo.gameObject);
            Destroy(CurrentPlayer);
        }

        private void EndState()
        {
            switch (_currentState)
            {
                case State.CONTROLS:
                    this.PanelControls.SetActive(false);
                    break;
                
                case State.EXIT:
                    this.PanelExit.SetActive(false);
                    break;

                case State.MENU:
                    this.PanelMenu.SetActive(false);
                    break;

                case State.LEVELCOMPLETE:
                    this.PanelLevelComplete.SetActive(false);
                    this.PanelPlay.SetActive(false);
                    break;

                case State.GAMEOVER:
                    this.PanelGameOver.SetActive(false);
                    this.PanelPlay.SetActive(false);
                    break;

                case State.SHOP:
                    this.PanelShop.SetActive(false);
                    break;

                default:
                    break;
            }
        }

        private void Initialize()
        {
            this.Camera.transform.position = _initialCameraPosition;
            CurrentCanvasTileInfo = Instantiate(this.CanvasTileInfo);
            CurrentField = Instantiate(this.GameFieldPrefab);
            CurrentPlayer = Instantiate(this.PlayerPrefab);
        }

        private void ReadKeyboardForExit()
        {
            if (Input.GetKeyUp(KeyCode.Y))
                Application.Quit();

            else if (Input.GetKeyUp(KeyCode.N))
                this.SwitchState(_previousState);
        }

        private void WaitForAnyKey()
        {
            if (Input.anyKeyDown)
            {
                this.DestroyGameObjects();
                this.SwitchState(State.MENU);
            }
        }

    }
}
